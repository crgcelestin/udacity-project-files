# Predict Bike Sharing Demand with AutoGluon

## Introduction to AWS Machine Learning Final Project


> ### [Project](project/autogluon_bike_sharing_demand.ipynb)
> ### [Kaggle Challenge](https://www.kaggle.com/competitions/bike-sharing-demand/overview)
> ### [Kaggle Dataset](https://www.kaggle.com/competitions/bike-sharing-demand/data)


<div align="center">
	<hr />
    <p>
        Detailed Description of the Coding Challenge
    </p>
    <hr />
</div>


- For the AI Module, Udacity tasked students to apply knowledge and methods learning in the Machine Learning course to compete in a Kaggle competition with the AutoGluon libarary. 

- We were tasked with downloading the Bike Sharing Demand dataset then training a model using AutoGluon on it
    - The first iteration was without any hyperparameters or additional features, we then submitted our intial results for this first iteration as a baseline
    - The second iteration required adding more features to the dataset which involved deserialization of the date and adding categories to the dataset in order for the model to distinguish between what are numbers and identifiable types/categories.
    - The final iteration required training a model with hyparameters that would enable extremely fitted results

> A report was written detailing this entire process and about what parameters/methods enabled better scoring on the kaggle dataset: [Report](project/bike_sharing_report.md)