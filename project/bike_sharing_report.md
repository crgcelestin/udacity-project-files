# Report: Predict Bike Sharing Demand with AutoGluon Solution
#### CRAIG CELESTIN

## Initial Training
### What did you realize when you tried to submit your predictions? What changes were needed to the output of the predictor to submit your results?

In terms of what I realized when attempting my predictions, was that I had to carefully follow the process. First was creating predictions using the .predict() then making sure that there were no negative values. I decided to explore my dataset to determine if there were any negative value occurences and then if if got a return True or found there was an occurence then I would have to use .get_numeric_data and then covert any occurences of negative values to 0 using 
```py
data[data<0]=0
```
then recheck. 
Following the first training part, for submission you have to create a csv off the sampleSubmission template for it that has the datetime column casted as a datetime object 
```py
submission['datetime'] = submission['datetime'].apply(pd.to_datetime)
```
Finally, create a 'count' column for the submissions csv that stores the generated predictions and cast that variable to a new csv with a title, index=False

### What was the top ranked model that performed?
WeightedEnsemble_L3 was the top ranked model as shown in the notebook when running predict.leaderboard()

## Exploratory data analysis and feature creation
### What did the exploratory analysis find and how did you add additional features?
The EDA allowed me to find what the initial data structure looks like. I was able to create month, hour, and year columns for each date and ensure that the models don't just think of types i.e season, weather as more than just numbers and when generating a new histogram i was able to getmore accurate distributions.

### How much better did your model preform after adding additional features and why do you think that is?
```
percent increase = [(final-inital) / (initial)] * 100
% increase = [(.68028-1.80969)/(1.80969)] *100 = −62.4090313811%
```
^ Represents a 62% accuracy increase when features were added opposed to prior which had the default features set

The model performed vastly better which is compounded when viewing the hpo. I believe that seperating the date into various columns allows the model to more accurately predict and categories allowing for finetuned classification.

## Hyper parameter tuning
### How much better did your model preform after trying different hyper parameters?
The model on the first try of hyper parameters was 1.80969 and after finetuning the hyperparameters and adding features it went down to .51098, which is a ~72% increase in accuracy. 

### If you were given more time with this dataset, where do you think you would spend more time?
I would spend more time in the hpo stage and EDA.

### Create a table with the models you ran, the hyperparameters modified, and the kaggle score.
|model|hpo1|hpo2|hpo3|score|
|--|--|--|--|--|
|initial|use_orig_features|max_base_models|max_base_models_per_type|1.80969|
|use_orig_features|max_base_models|max_base_models_per_type|presets|0.68028|
|hpo|learning_rate| num_boost_round|num_leaves| .51098|

### Create a line plot showing the top model score for the three (or more) training runs during the project.

TODO: Replace the image below with your own.

![model_train_score.png](img/top_model_scores2.png)

### Create a line plot showing the top kaggle score for the three (or more) prediction submissions during the project.

TODO: Replace the image below with your own.

![model_test_score.png](img/top_kaggle_scores2.png)

## Summary
The first chart shows that in fact that the first entry will be the 'best' model as it relates to model score during training runs, however the second chart shows a trend where the kaggle score decreases as we trend towards adding features and finetuning hyperparameters. 

